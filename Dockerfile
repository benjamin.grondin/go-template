FROM golang:1.17

ARG CGO_ENABLED=1
ARG GOOS=linux
ARG GOARCH=amd64
ARG SERVICE=app
ARG PORT=5000/tcp

WORKDIR /go/src/app/${SERVICE}

COPY cmd ./cmd
COPY internal ./internal
COPY pkg ./pkg

RUN CGO_ENABLED=${CGO_ENABLED} GOOS=${GOOS} GOARCH=${GOARCH} \
    go build -o bin/${SERVICE} cmd/${SERVICE}/main.go

# 
# 
FROM alpine:3.15

WORKDIR /app

COPY --from=builder /go/src/app/${SERVICE}/bin/${SERVICE} .

EXPOSE ${PORT}

CMD [ "/app/${SERVICE}" ]