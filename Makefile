# Setup
SERVICE=app

#
VERSION=$(shell cat VERSION)
ENV_FILE=local.env
FLAGS=

#
CLEANABLE=	bin \
			.cache

#
MAIN=cmd/$(SERVICE)/main.go
BUILD=bin/$(SERVICE)

# Go build
CGO_ENABLED=1
GOOS=linux
GOARCH=amd64



# Display version of repositorie
app-version:
	@echo "$(SERVICE) - $(VERSION)."

# Run
run:
	go run $(MAIN) $(FLAGS)

# Build
build:
	CGO_ENABLED=$(CGO_ENABLED) GOOS=$(GOOS) GOARCH=$(GOARCH) \
	go build --ldflags "-X main.AppVersion=$(VERSION)" \
	-o $(BUILD) $(MAIN)

# Check dependencies
dependencies:
	go mod tidy && go mod vendor

# Clean repositorie
clean:
	rm -Rf $(CLEANABLE)

# Test
test: clean
	go test ./... -cover -v -cover

swagger:
# export PATH=$(go env GOPATH)/bin:$PATH
	go get -u github.com/swaggo/swag/cmd/swag

# Update swagger api
updoc:
	swag init -g $(MAIN) --parseDependency --parseInternal

# Format swagger comment
fordoc:
	swag fmt